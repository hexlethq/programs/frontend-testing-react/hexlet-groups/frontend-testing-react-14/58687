import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { prettyDOM, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';
const countries = ['afghanistan', 'albania', 'algeria'];

beforeAll(() => {
  nock.disableNetConnect();
});

// BEGIN
test('input renders', () => {
  const { getByRole } = render(<Autocomplete />);
  expect(getByRole('textbox')).toBeInTheDocument();
});
test('user input', async () => {
  nock(host)
    .persist()
    .get('/countries')
    .query(true)
    .reply(200, ((uri) => {
      const params = (new URL(uri, host)).searchParams;
      const term = params.get('term').toLowerCase();
      const filtered = countries.filter((country) => country.startsWith(term));
      return filtered;
    }));
  const { getByRole, findByText, queryByRole } = render(<Autocomplete />);
  const input = getByRole('textbox');

  userEvent.type(input, 'a');

  const afghanistan = await findByText(/afghanistan/i);
  const albania = await findByText(/albania/i);
  const algeria = await findByText(/algeria/i);

  await waitFor(() => {
    expect(getByRole('list')).toContainElement(afghanistan);
    expect(getByRole('list')).toContainElement(albania);
    expect(getByRole('list')).toContainElement(algeria);
  });

  userEvent.type(input, 'l');
  await waitFor(() => {
    expect(getByRole('list')).not.toContainElement(afghanistan);
    expect(getByRole('list')).toContainElement(albania);
    expect(getByRole('list')).toContainElement(algeria);
  });

  userEvent.type(input, 'b');
  await waitFor(() => {
    expect(getByRole('list')).not.toContainElement(afghanistan);
    expect(getByRole('list')).toContainElement(albania);
    expect(getByRole('list')).not.toContainElement(algeria);
  });

  userEvent.clear(input);
  await waitFor(() => {
    expect(queryByRole('list')).toBeNull();
  });
});
// END
