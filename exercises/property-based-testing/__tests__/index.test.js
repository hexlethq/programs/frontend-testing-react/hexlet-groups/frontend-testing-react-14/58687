const fc = require("fast-check");

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe("Property based tests", () => {
  it("Sorted array length should be the same with original data", () => {
    fc.assert(
      fc.property(fc.int32Array(), (data) =>
        expect(data.length).toEqual(sort(data).length)
      )
    );
  });
  it("Double sorted array is equal to singe sorted one", () => {
    fc.assert(
      fc.property(fc.int32Array(), (data) => {
        const sorted = sort(data);
        const doubleSorted = sort(sort(data));
        expect(sorted).toEqual(doubleSorted);
      })
    );
  });
  it("Sorted array should be sorted", () => {
    fc.assert(
      fc.property(fc.int32Array(), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      })
    );
  });
});
// END
