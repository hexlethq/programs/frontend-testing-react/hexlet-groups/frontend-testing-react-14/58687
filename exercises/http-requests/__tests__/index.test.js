const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');
const users = require('../fixtures/users');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
describe('Endpoints tests', () => {
  beforeAll(() => {
    nock.disableNetConnect();
  });

  afterAll(() => {
    nock.enableNetConnect();
  });

  it('GET /users', async () => {
    nock('https://example.com').get('/users').reply(200, users);
    const response = await get('https://example.com/users');
    expect(response).toMatchObject(users);
  });

  it('Check GET error', async () => {
    nock('https://example.com').get('/users').reply(404);
    await expect(get('https://example.com/users')).rejects.toThrow();
  });

  it('POST /users', async () => {
    const id = { id: '123ABC' };
    nock('https://example.com').post('/users').reply(201, id);
    const response = await post('https://example.com/users', users[0]);
    expect(response).toMatchObject(id);
  });

  it('Check POST error', async () => {
    nock('https://example.com').post('/users').reply(500);
    await expect(get('https://example.com/users')).rejects.toThrow();
  });
});
// END
