const axios = require('axios');

// BEGIN
const get = async (endpooint) => {
  const resp = await axios.get(endpooint);
  return resp.data;
};

const post = async (endpoint, data) => {
  const resp = await axios.post(endpoint, data);
  return resp.data;
};
// END

module.exports = { get, post };
