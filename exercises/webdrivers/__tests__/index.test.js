const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
  });

  beforeEach(async () => {
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 10,
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  it('Main page test', async () => {
    await page.goto(appUrl);
    await expect(page.title()).resolves.toMatch('Simple Blog');
    const header = (await page.$eval('#title', (title) => title.textContent));
    expect(header).toBe('Welcome to a Simple blog!');
  });

  it('Articles', async () => {
    await page.goto(appArticlesUrl);
    const header = (await page.$eval('h3', (hdr) => hdr.textContent));
    expect(header).toBe('Articles');
    const tableSize = await page.$$eval('table', (elements) => elements.length);
    expect(tableSize).not.toBeFalsy();
  });
  it('Create and change article', async () => {
    await page.goto(appArticlesUrl);
    await page.click('.container.mt-3 > a');
    await page.waitForSelector('form');
    const form = await page.$$eval('form', (elements) => elements.length);
    expect(form).not.toBeFalsy();

    await page.type('#name', 'new article');
    await page.select('select', '1');
    await page.type('#content', 'This is a new awesome article');
    await page.click('[type="submit"]');
    await page.waitForSelector('#articles');
    const articleNames = await page.$$eval('td', (tds) => tds.map((td) => td.textContent));
    expect(articleNames).toContain('new article');

    const tableSnapshot = await page.$eval('table', (table) => table.textContent);
    await page.click('td > a');
    await page.waitForSelector('#name');
    await page.type('#name', 'abrakadabra');
    await page.click('[type="submit"]');
    await page.waitForSelector('#articles');
    const updatedTableSnapshot = await page.$eval('table', (table) => table.textContent);
    expect(updatedTableSnapshot).not.toEqual(tableSnapshot);
  });
  // END

  afterEach(async () => {
    await browser.close();
  });

  afterAll(async () => {
    await app.close();
  });
});
