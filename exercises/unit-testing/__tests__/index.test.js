describe('Object.assign tests', () => {
  test('Basic', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);
    expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  });
  test('No parameters', () => {
    expect(() => Object.assign()).toThrow();
  });
  test('Empty target, no src', () => {
    const target = {};
    expect(Object.assign(target)).toEqual({});
  });
  test('Empty target and src', () => {
    const src = {};
    const target = {};
    expect(Object.assign(target, src)).toEqual({});
  });
  test('Empty src', () => {
    const src = {};
    const target = { a: 'a' };
    expect(Object.assign(target, src)).toEqual({ a: 'a' });
  });
  test('Target and src are equal', () => {
    const src = { a: 'a' };
    const target = { a: 'a' };
    expect(Object.assign(target, src)).toEqual({ a: 'a' });
  });
  test('Target and src are different', () => {
    const src = { b: 'b' };
    const target = { a: 'a' };
    expect(Object.assign(target, src)).toEqual({ a: 'a', b: 'b' });
  });
  test('Target assignment error', () => {
    const src = { a: 'b' };
    const target = {};
    Object.defineProperty(target, 'a', {
      value: 'a',
      writable: false,
    });
    expect(() => Object.assign(target, src)).toThrow();
  });
  test('Check origin mutation', () => {
    const target = { a: 'a' };
    const result = Object.assign(target, { a: 'b' });
    expect(result).toBe(target);
  });
});
