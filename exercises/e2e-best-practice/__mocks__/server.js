import { setupWorker, rest } from 'msw';

let tasks = [];
let id = 0;

export default setupWorker(
  rest.get('http://localhost:8080/tasks', (req, res, ctx) => res(
    ctx.status(200),
    ctx.json(tasks),
  )),
  rest.post('http://localhost:8080/tasks', (req, res, ctx) => {
    const { task: { text } } = req.body;
    const newTask = { id, state: 'active', text };
    tasks.push(newTask);
    id += 1;
    return res(
      ctx.status(200),
      ctx.json(newTask),
    );
  }),
  rest.delete('http://localhost:8080/tasks/:uuid', (req, res, ctx) => {
    const { uuid } = req.params;
    tasks = tasks.filter((task) => task.uuid !== uuid);
    return res(
      ctx.status(204),
    );
  }),
);
