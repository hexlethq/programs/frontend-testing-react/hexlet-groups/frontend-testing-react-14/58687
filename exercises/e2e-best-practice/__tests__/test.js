// @ts-nocheck
import faker from 'faker';

const appUrl = 'http://localhost:8080';
const timeout = 15000;

const addTask = async (text) => {
  await expect(page).toFill('[data-testid="task-name-input"]', text);
  await expect(page).toClick('[data-testid="add-task-button"]');
};

test('Main', async () => {
  await page.goto(appUrl);
  await expect(page).toMatchElement('[data-testid="task-name-input"]');
  await expect(page).toMatchElement('[data-testid="add-task-button"]');

  const firstTask = faker.lorem.lines(1);
  await addTask(firstTask);
  await expect(page).toMatch(firstTask);

  const secondTask = faker.lorem.lines(1);
  await addTask(secondTask);
  await expect(page).toMatch(secondTask);

  await expect(page).toClick('[data-testid*="remove-task-1"]');
  await expect(page).not.toMatch(secondTask);
}, timeout);
