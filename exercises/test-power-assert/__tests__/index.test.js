const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const testData = [1, [2, [3, [4]], 5]];
assert.deepEqual(flattenDepth(testData), [1, 2, [3, [4]], 5]);
assert.deepEqual(flattenDepth(testData, 2), [1, 2, 3, [4], 5]);
assert.deepEqual(flattenDepth(testData, 10), [1, 2, 3, 4, 5]);
assert.deepEqual(flattenDepth(testData, 'string'), [1, [2, [3, [4]], 5]]);
// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
