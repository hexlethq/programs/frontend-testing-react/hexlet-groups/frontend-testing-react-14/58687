require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

testingLibraryDom.configure({ testIdAttribute: 'data-container' });

const {
  findByText, getAllByRole, queryAllByRole, getByText,
} = require('@testing-library/dom');
const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

const addTask = (taskname) => {
  const inputField = screen.getByLabelText('New Task Name');
  userEvent.type(inputField, taskname);
  userEvent.click(screen.getByText('Add Task'));
};

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
it('init state', async () => {
  const lists = screen.getByTestId('lists');
  const listItems = getAllByRole(lists, 'listitem');
  expect(await findByText(lists, 'General')).toBeVisible();
  expect(listItems.length).toBe(1);
  const tasks = screen.getByTestId('tasks');
  const taskItems = queryAllByRole(tasks, 'listitem');
  expect(taskItems.length).toBe(0);
});

it('add a task to default list', async () => {
  const inputField = screen.getByLabelText('New Task Name');
  const tasks = screen.getByTestId('tasks');
  addTask('Task 1');
  expect(getByText(tasks, 'Task 1')).toBeVisible();
  expect(inputField.value).toBe('');
  const defaultList = screen.getByText('General');
  expect(defaultList.nodeName).toBe('B');
  addTask('Task 2');
  const taskItems = getAllByRole(tasks, 'listitem');
  expect(taskItems.length).toBe(2);
  expect(getByText(tasks, 'Task 2')).toBeVisible();
});

it('add tasks to a new list', async () => {
  userEvent.type(screen.getByLabelText('New List Name'), 'New list');
  userEvent.click(screen.getByText('Add List'));
  expect(screen.getByText('New list')).toBeVisible();
  addTask('Task for a General list');
  expect(screen.getByText('Task for a General list')).toBeVisible();
  userEvent.click(screen.getByText('New list'));
  expect(screen.queryByText('Task for a General list')).toBe(null);
});
// END
