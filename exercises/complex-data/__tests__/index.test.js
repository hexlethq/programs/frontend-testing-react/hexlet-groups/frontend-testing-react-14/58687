const faker = require('faker');

// BEGIN
describe('createTransaction test', () => {
  let transaction;
  beforeEach(() => {
    transaction = faker.helpers.createTransaction();
  });
  test('Datastructure check', () => {
    expect(transaction).toEqual({
      date: expect.any(Date),
      account: expect.any(String),
      amount: expect.any(String),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
    });
  });
  test('Uniqueness check', () => {
    expect(transaction).not.toStrictEqual(faker.helpers.createTransaction());
  });
});
// END
